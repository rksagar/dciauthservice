package com.org.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig{
	/*
	 * @Bean public Docket productApi() { return new
	 * Docket(DocumentationType.SWAGGER_2) .select()
	 * .apis(RequestHandlerSelectors.basePackage("com.org.controller"))
	 * .paths(regex("/clientpost.*")) .build() .apiInfo(metaData()); }
	 */
	
	
	  @Bean
	  public Docket postsApi() 
	  { 
		  return new Docket(DocumentationType.SWAGGER_2).groupName("public-api")
	  .apiInfo(apiInfo()).select().paths(postPaths()).build(); }
	  
	  private Predicate<String> postPaths() { return (regex("/DCI/.*")); }
	 
	
	/*
	 * return new Docket(DocumentationType.SWAGGER_2) .select()
	 * .apis(RequestHandlerSelectors.basePackage("com.org.controller"))
	 * //RequestHandlerSelectors.basePackage("controller")
	 * .paths(regex("/DCI/clientpost.*")) //.paths(regex("/persons.*"))
	 * paths(PathSelectors.any()) .build();
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("JavaInUse API")
		.description("JavaInUse API reference for developers")
		.termsOfServiceUrl("http://javainuse.com")
		.contact("javainuse@gmail.com").license("JavaInUse License")
		.licenseUrl("javainuse@gmail.com").version("1.0").build();
		}
	
	
	/*
	 * private ApiInfo metaData() { return new ApiInfoBuilder()
	 * .title("Spring Boot REST API")
	 * .description("\"Spring Boot REST API for Online Store\"") .version("1.0.0")
	 * .license("Apache License Version 2.0")
	 * .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0\"") .contact(new
	 * Contact("John Thompson", "https://springframework.guru/about/",
	 * "john@springfrmework.guru")) .build(); }
	 */
	/*
	 * @Override protected void addResourceHandlers(ResourceHandlerRegistry
	 * registry) { registry.addResourceHandler("swagger-ui.html")
	 * .addResourceLocations("classpath:/META-INF/resources/");
	 * 
	 * registry.addResourceHandler("/webjars/**")
	 * .addResourceLocations("classpath:/META-INF/resources/webjars/"); }
	 */
}
