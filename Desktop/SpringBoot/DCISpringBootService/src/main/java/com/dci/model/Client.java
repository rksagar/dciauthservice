package com.org.model;

public class Client {

	private int clientId;
	private String cName;
	private String pass;
	public Client() {
		
	}

	public Client(int clientId, String cName) {
		super();
		this.clientId = clientId;
		this.cName = cName;
	}

	
	
	

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}
	
	
	
}
