package com.org.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.dao.ClientDao;
import com.org.model.Client;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientDao cdao;
	
	
	public Object getClients() {
		
		return cdao.getClientList();
	}

  
	
	
	
}
