package com.org.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.org.dao.ClientDao;


@Service
public class AuthenticationServiceImpl implements AuthenticationService {
	
    @Autowired 
	private ClientDao dao;
    
    
    
	@Override
	public Map authenticateUser(String userId, String passWord) {
		Map<String,String> dataMap = new HashMap<String,String>();
		String data = "";
		Map userSaltMap = dao.getUserSaltDetails(userId);
		String userSaltValue = (String) userSaltMap.get("FSALT");
		String userDigest = "";
		if(userSaltValue != null && !userSaltValue.trim().equals("")){
			UserAuthenticationDigest userAuthenticationDigest = new UserAuthenticationDigest(); 
			userDigest = userAuthenticationDigest.getUserDigest(userId, passWord, userSaltValue);
			Map authenticationDetails= dao.authenticateUserDB(userId, userDigest);
			//authenticationDetails.put("Token","");
			
			if (authenticationDetails != null && authenticationDetails.get("HFMSGID") != null) {
				String msgId = authenticationDetails.get("HFMSGID").toString();
				String userInternalId = authenticationDetails.get("HFUSERID").toString();
				String msg = (String) authenticationDetails.get("HFMSG");
				if (msgId != null) {
					if((msgId.trim().equals("-1") || msgId.trim().equals("2"))){
						data = "Invalid password";
					}else if((msgId.trim().equals("-2") || msgId.trim().equals("3"))){
						data = "Invalid user";
					}else if(msgId.trim().equals("5")){
						data = "Force Change Password";
					}else if(msgId.trim().equals("6")){
						data = "User locked";
					} else if (msgId.trim().equals("99")) {
					    data = "User/password expired";
					} else if (msgId.trim().equals("7")) {
					    data = "User deactivated";
					}else if(msgId.trim().equals("0")){
						data = "success";
						dataMap.put("Token", "");
						//Map<String,String> userDetails = new HashMap<String,String>();
						//userDetails.put("userid", userId);
						/* String userSalt = dao.getUserSalt(userId); */
						/*
						 * Map<String,String> tokensMap =
						 * jwtAccessRefreshTokenService.generateAccessRefreshTokens(userDetails,userSalt
						 * ); if(tokensMap != null && tokensMap.containsKey("accessToken") &&
						 * tokensMap.containsKey("refreshToken")){ String accessToken =
						 * tokensMap.get("accessToken"); String refreshToken =
						 * tokensMap.get("refreshToken"); String refreshTokenExpTime =
						 * tokensMap.get("refreshTokenExpTime"); String refreshTokenExpDateTime =
						 * tokensMap.get("refreshTokenExpDateTime"); boolean status =
						 * authenticationservice.saveTokenInfoDB(userid, GUID, accessToken,
						 * refreshToken, refreshTokenExpTime, refreshTokenExpDateTime, 1); if(status){
						 * data =
						 * "successaccesstokenstart~~~~#####"+accessToken+"accesstokenend~~~~#####";
						 * }else{ result = new ResponseEntity<String>("signontokenerror",
						 * HttpStatus.OK); } }
						 */
					}
				} 
				dataMap.put("msg", data);
			}
			
		return dataMap;
		}else if(userSaltValue != null && userSaltValue.trim().equals("")){
			//This block is when user salt is not found i.e user not found
			Map authenticationDetails = new HashMap();
			authenticationDetails.put("msgId", 3);
			authenticationDetails.put("userId", 0);
			authenticationDetails.put("msg", "");
			return authenticationDetails;
		}
		return null;
		
	}

}
