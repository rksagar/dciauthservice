package com.org.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.org.model.Client;

@Repository
public class ClientDao {

	@Autowired
	private DataSource dataSource;

	private SimpleJdbcCall jdbcCall;
	private JdbcTemplate temp;
	private SimpleJdbcCall jdbcCallTwo;

	@PostConstruct
	public void init() {


		JdbcTemplate template = new JdbcTemplate(dataSource);
		// jdbcCall = new
		// SimpleJdbcCall(template).withSchemaName("ZDBXLM0005").withProcedureName("SP_UTGETCLIENTLIST").declareParameters(buildSqlParameters());
		jdbcCall = new SimpleJdbcCall(template).withSchemaName("ZDBXAUTH04").withProcedureName("SP7_DSGETSALT")
				.declareParameters(buildSqlParameters());
		jdbcCallTwo = new SimpleJdbcCall(template).withSchemaName("ZDBXAUTH04").withProcedureName("SP7_DSLOGIN")
				.declareParameters(buildSqlParametersTwo());
	}


	public SqlParameter[] buildSqlParameters() {
		return new SqlParameter[] {
				// new SqlParameter("FCLIENTID", Types.BIGINT),
				// new SqlParameter("FCLIENT_NAME", Types.VARCHAR),
				new SqlParameter("HFUSER_ID", Types.VARCHAR) };
	}

	public SqlParameter[] buildSqlParametersTwo() {
		return new SqlParameter[] {
				// new SqlParameter("FCLIENTID", Types.BIGINT),
				new SqlParameter("HFUSER_ID", Types.VARCHAR), new SqlParameter("HFPWORD", Types.VARCHAR) };
	}

	public Map<String, String> getUserSaltDetails(String userId) {

		Map<String, String> map = new HashMap<String, String>();
		
		Map<String, String> userSaltMap = new HashMap();
		List<String> list = new ArrayList<String>();
		// JdbcTemplate templ = new JdbcTemplate(dataSource);
		// SimpleJdbcCall jdbcCallOne = new
		// SimpleJdbcCall(template).withSchemaName("ZDBXAUTH04").withProcedureName("SP7_DSGETSALT(?)").declareParameters(buildSqlParameters());

		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put("HFUSER_ID", userId);

		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
		Map<String, Object> out = jdbcCall.execute(in);

		Iterator<Entry<String, Object>> it = out.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
			String key = (String) entry.getKey();
			Object value = (List) entry.getValue();

			String result = value.toString().replaceAll("[{\\[\\]}]", "");
			
			/* userSaltMap=(Map)value; */
			String[] splited = result.split(",");

			for (String str : splited) {
				String[] arr = str.split("=");
				map.put(arr[0].trim(), arr[1].trim());
			}
			
		}

		return map;
	}
	public String getUserSalt(String userId){
		String salt = "";
		Map userSaltMap = getUserSaltDetails(userId);
		if(userSaltMap.containsKey("FSALT")){
			salt = (String) userSaltMap.get("FSALT");
		}
		return salt;
	}

	public Map authenticateUserDB(String username, String password) {
		
		Map authenticationDetails = new HashMap();

		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put("HFUSER_ID", username);
		inParamMap.put("HFPWORD", password);

		SqlParameterSource in = new MapSqlParameterSource(inParamMap);

		Map<String, Object> out = jdbcCallTwo.execute(in);

		Iterator<Entry<String, Object>> it = out.entrySet().iterator();
		
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
			String key = (String) entry.getKey();
			Object value = (Object) entry.getValue();
			
				 authenticationDetails.put(key, value); 
		}
		return authenticationDetails;
	}

	public Object getClientList() {

		List<Client> clients = new ArrayList<Client>();

		try {

			Map<String, Object> inParamMap = new HashMap<String, Object>();
			inParamMap.put("HFUSER_ID", "dcisupportlm");

			SqlParameterSource in = new MapSqlParameterSource(inParamMap);

			Object value = null;
			// SqlParameterSource in = new MapSqlParameterSource().addValue("HFUSER_ID",
			// "dcisupportjhf");
			Map<String, Object> out = jdbcCall.execute(in);

			Iterator<Entry<String, Object>> it = out.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
				String key = (String) entry.getKey();
				value = (Object) entry.getValue();

				System.out.println("Key: " + key);
				System.out.println("Value: " + value);
			}

			System.out.println("ClientName:" + out.get("FCLIENTID"));
			System.out.println("ClientName:" + out.size());

			return value;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
