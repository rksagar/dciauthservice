package com.org.model;

public class User {

	private String userName;
	private String pass;
	private String clientId;
	
	
	public User(String userName, String pass, String clientId) {
		super();
		this.userName = userName;
		this.pass = pass;
		this.clientId = clientId;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPass() {
		return pass;
	}


	public void setPass(String pass) {
		this.pass = pass;
	}


	public String getClientId() {
		return clientId;
	}


	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	
	
}
