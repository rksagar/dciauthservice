package com.org.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.org.model.Client;
import com.org.service.AuthenticationService;
import com.org.service.AuthenticationServiceImpl;
import com.org.service.ClientServiceImpl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@RestController
public class ClientController {

	@Autowired
	private ClientServiceImpl csobj;
	
	@Autowired
	private AuthenticationServiceImpl authServObj;
	
	@GetMapping("/DCI/clientpost/")
	public Object getClients()
	{
		
		return csobj.getClients();
	}
	
	@PostMapping("/auth/login/")
	public Object login(@RequestParam("userId")String userId,@RequestParam("password")String password)
	{
		Object obj=null;
		ResponseEntity<String> result = null;
		if(userId!=null && password!=null)
		{
			obj=authServObj.authenticateUser(userId, password);
			
		}
		return obj;
	}

  @PostMapping("/DCI/tokenpost/")
  public Object getToken(@RequestParam String token) { 
	 
	  final String uri = "http://192.168.1.23:8080/jwt/{token}";
	     
	  
	    Map<String, String> params = new HashMap<String, String>();
	    params.put("token", token);
	     
	    RestTemplate restTemplate = new RestTemplate();
	   String result = restTemplate.getForObject(uri,String.class,params);
	    if(result.equalsIgnoreCase("valid"))
	    {
	    	return csobj.getClients();
	    }
	    else
	    {
	    	return null;
	    }
	   
  } 
  
}
 
//}