package com.org.service;

import java.util.HashMap;
import java.util.List;

import org.jasypt.contrib.org.apache.commons.codec_1_3.binary.Base64;
import org.jasypt.digest.StandardStringDigester;
import org.jasypt.salt.RandomSaltGenerator;
import org.jasypt.salt.StringFixedSaltGenerator;

public class UserAuthenticationDigest {

	
	private HashMap departmentConfig;
	//DciCommon dcicom = new DciCommon();

	final static int saltSize = 24;
	/*private String algorithm = "";
	private String iterations = "";*/



	public String saltGenerator(){
		RandomSaltGenerator randomSaltGenerator = new RandomSaltGenerator();
		byte[] randomSalt = randomSaltGenerator.generateSalt(saltSize);
		//String randomSaltStr = new String(randomSalt);
		randomSalt = Base64.encodeBase64(randomSalt);
		String randomSaltStr = new String(randomSalt);
		//return randomSaltStr;
		return randomSaltStr;
	}

	public StandardStringDigester getDigester(String saltValue){
		/*if(dcicom.valueFromPreConfig("algorithm") != null && dcicom.valueFromPreConfig("iterations") != null && !dcicom.valueFromPreConfig("algorithm").trim().equals("") && !dcicom.valueFromPreConfig("iterations").trim().equals("")){
			String algorithmValue = dcicom.valueFromPreConfig("algorithm");
			String iterationsValue = dcicom.valueFromPreConfig("iterations");
			String actualAlgorithm = Base64CoderUtil.decodeString(algorithmValue);
			String actualIterations = Base64CoderUtil.decodeString(iterationsValue);
			StandardStringDigester digester = new StandardStringDigester();
			StringFixedSaltGenerator fixedSaltValue = new StringFixedSaltGenerator(saltValue);
			digester.setAlgorithm(actualAlgorithm);   
			digester.setIterations(Integer.parseInt(actualIterations)); 
			digester.setSaltGenerator(fixedSaltValue);
			return digester;
		}
		else{
			return null;
		}*/
		
		String algorithmValue = "SHA-512";
		String iterationsValue = "50000";
		//String actualAlgorithm = com.dci.ComponentInfo.utilities.Base64CoderUtil.decodeString(algorithmValue);
		//String actualIterations = com.dci.ComponentInfo.utilities.Base64CoderUtil.decodeString(iterationsValue);
		String actualAlgorithm = algorithmValue;
		String actualIterations = iterationsValue;
		StandardStringDigester digester = new StandardStringDigester();
		StringFixedSaltGenerator fixedSaltValue = new StringFixedSaltGenerator(saltValue);
		digester.setAlgorithm(actualAlgorithm);   
		digester.setIterations(Integer.parseInt(actualIterations)); 
		digester.setSaltGenerator(fixedSaltValue);
		return digester;

	}

	public String digester(String userPassword , String salt){
		StandardStringDigester digester = getDigester(salt);
		if(digester != null){
			String digest = digester.digest(userPassword);
			return digest;
		}
		else{
			return "";
		}
	}

	public String getUserDigestValue(String inputSaltPassword , String salt) {
		StandardStringDigester digester = getDigester(salt);
		if(digester != null){
			String digest = digester.digest(inputSaltPassword);
			return digest;
		}
		else{
			return "";
		}

	}
	public String getUserDigest(String userId, String password, String salt) {
		// get the users salt from the database with help of userID
		if (salt != null && !salt.trim().equals("")) {
			String saltedPassword = salt + password;
			String digest  = getUserDigestValue(saltedPassword , salt);
			return digest;
		} 
		else{
			return "noUser";
		}
	}

	public HashMap<String, String> newUserDigest(String password){
		String newSalt = saltGenerator();
		String saltedPassword = newSalt + password;
		String digest = digester(saltedPassword , newSalt);
		HashMap<String, String> saltMap = new HashMap<String, String>();
		saltMap.put("SALT", newSalt);
		saltMap.put("DIGEST", digest);
		return saltMap;
	}

	public String getUsersPreviousDigestString(List<String> previousUsedSalts, String inputPassword){
		String concatenatedDigests = "";
		String SEPERATOR = "$#$#$#$#$#";
		for(String saltValue : previousUsedSalts){
			String saltPassword = saltValue + inputPassword;
			String digest = digester(saltPassword , saltValue);
			//developerLog.debug("digest \n"+digest+"\n");
			concatenatedDigests = concatenatedDigests + SEPERATOR + digest;
		}
		if(concatenatedDigests.startsWith(SEPERATOR)){
			concatenatedDigests = concatenatedDigests.substring(SEPERATOR.length());
		}
		//developerLog.debug("concatenatedDigests \n"+concatenatedDigests+"\n");
		return concatenatedDigests;
}
	}
