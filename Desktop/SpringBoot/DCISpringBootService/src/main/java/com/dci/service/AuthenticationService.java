package com.org.service;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public interface AuthenticationService {

	public Map authenticateUser(String userId, String passWord);
}
