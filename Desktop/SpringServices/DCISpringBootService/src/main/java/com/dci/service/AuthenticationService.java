package com.dci.service;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public interface AuthenticationService {

	public Map authenticateUser(String userId, String passWord);
	public Object getPrivilegeInfo(String userId,String clientId);
	
}
