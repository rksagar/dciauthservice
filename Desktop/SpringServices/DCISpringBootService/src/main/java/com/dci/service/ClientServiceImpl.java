package com.dci.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dci.dao.ServiceDao;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ServiceDao cdao;

	public Object getClients() {

		return cdao.getClientList();
	}

}
