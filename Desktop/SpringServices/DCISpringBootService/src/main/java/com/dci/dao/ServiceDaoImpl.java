package com.dci.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.dci.model.Group;
import com.dci.service.AuthenticationServiceImpl;

@Repository
public class ServiceDaoImpl  {
	private static final Logger LOGGER = LogManager.getLogger(ServiceDaoImpl.class);
	
	@Autowired
	private DataSource dataSource;
	@Autowired
    private JdbcTemplate jdbcTemplateObject;
    
    
  
    
    public Object getPrivileges(String userId,String clientId)
    {
    	LOGGER.info("Entering into com.dci.service.AuthenticationServiceImpl.getPrivileges(String userId, String clientId)");
    	String sql="call SP7_DSGETUSERPERMISION(?,?,?)";
    	//String sql="execute SP7_DSGETGROUPPRIVILEGE ?,?,?,?,?";
    	Object search[]={"6422",clientId,"10"};
    	Group group=new Group(clientId,userId);
    	 HashMap<String,Boolean> TextMap=new HashMap();
	      HashMap<String,Boolean> SubheadMap=new HashMap();
	      HashMap<String,Boolean> FootnoteMap=new HashMap();
	      HashMap<String,Boolean> TableMap=new HashMap();
	      HashMap<String,Boolean> TitleMap=new HashMap();
	      HashMap<String,Boolean> FootNote_MappingMap=new HashMap();
	      HashMap<String,Boolean> Fund_ManagementMap=new HashMap();
	      
             jdbcTemplateObject.query(sql,search,new RowMapper(){
        	 @Override
        	 public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        		 
        		 String ObjectDesc=rs.getString("FPRIV_OPTION_DESC");
 				
				 String PrivilegeDesc=rs.getString("FPRIV_OPTION_DESC");
				 String SelectedInd=rs.getString("FSELECTED_IND");
				
				
				if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Text")) {

					if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd.equals("1")) {

						TextMap.put("CREATE", true);
						group.setText(TextMap);

					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd.equals("1")) {

						TextMap.put("EDIT", true);
						group.setText(TextMap);

					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd.equals("1")) {

						TextMap.put("VIEW", true);
						group.setText(TextMap);

					}
				} else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Subhead")) {

					if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd.equals("1")) {
						SubheadMap.put("CREATE", true);
						group.setSubhead(SubheadMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd.equals("1")) {
						SubheadMap.put("EDIT", true);
						group.setSubhead(SubheadMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd.equals("1")) {
						SubheadMap.put("VIEW", true);
						group.setSubhead(SubheadMap);
					}
				} else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Footnote") && SelectedInd.equals("1")) {

					if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd.equals("1")) {
						FootnoteMap.put("CREATE", true);
						group.setFootnote(FootnoteMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd.equals("1")) {
						FootnoteMap.put("EDIT", true);
						group.setFootnote(FootnoteMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd.equals("1")) {
						FootnoteMap.put("VIEW", true);
						group.setFootnote(FootnoteMap);
					}
				}

				else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Title") && SelectedInd.equals("1")) {

					if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd.equals("1")) {
						TitleMap.put("CREATE", true);
						group.setTitle(TitleMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd.equals("1")) {
						TitleMap.put("EDIT", true);
						group.setTitle(TitleMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd.equals("1")) {
						TitleMap.put("VIEW", true);
						group.setTitle(TitleMap);
					}
				} else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Table") && SelectedInd.equals("1")) {

					if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd.equals("1")) {
						TableMap.put("CREATE", true);
						group.setTable(TableMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd.equals("1")) {
						TableMap.put("EDIT", true);
						group.setTable(TableMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd.equals("1")) {
						TableMap.put("VIEW", true);
						group.setTable(TableMap);
					}
				}

				else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Footnote Mapping")
						&& SelectedInd.equals("1")) {

					if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd.equals("1")) {
						FootNote_MappingMap.put("CREATE", true);
						group.setFootnote_Mapping(FootNote_MappingMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd.equals("1")) {
						FootNote_MappingMap.put("EDIT", true);
						group.setFootnote_Mapping(FootNote_MappingMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd.equals("1")) {
						FootNote_MappingMap.put("VIEW", true);
						group.setFootnote_Mapping(FootNote_MappingMap);
					}
				}

				else if (rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Fund Management")
						&& SelectedInd.equals("1")) {

					if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("CREATE") && SelectedInd.equals("1")) {
						Fund_ManagementMap.put("CREATE", true);
						group.setFund_Management(Fund_ManagementMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("EDIT") && SelectedInd.equals("1")) {
						Fund_ManagementMap.put("EDIT", true);
						group.setFund_Management(Fund_ManagementMap);
					} else if (rs.getString("FPRIV_OPTION_DESC").equalsIgnoreCase("VIEW") && SelectedInd.equals("1")) {
						Fund_ManagementMap.put("VIEW", true);
						group.setFund_Management(Fund_ManagementMap);
					}
				}
				else
					if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("View Library")&& SelectedInd.equals("1"))
					{
						group.setLibrary(true);
					}
					else
						if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Context")&& SelectedInd.equals("1"))
						{
							group.setContext(true);
						}
						else
							if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("All Charts Status")&& SelectedInd.equals("1"))
							{
								group.setCharts(true);
							}
							else
								if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Open Data File")&& SelectedInd.equals("1"))
								{
									group.setOpenDataFile(true);
								}
								else
									if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Upload Quantative")&& SelectedInd.equals("1"))
									{
										group.setUploadDataFile(true);
									}
									else
										if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Change Status")&& SelectedInd.equals("1"))
										{
											group.setChangeStatus(true);
										}
										else
											if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Carry Forward")&& SelectedInd.equals("1"))
											{
												group.setCarryForward(true);
											}
											else
												if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Proof Document")&& SelectedInd.equals("1"))
												{
													group.setProof(true);
												}
												else
													if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Service Desk Admin")&& SelectedInd.equals("1"))
													{
														group.setServiceDesk(true);
													}
													else
														if(rs.getString("FDCIOBJECT_DESC").equalsIgnoreCase("Report")&& SelectedInd.equals("1"))
														{
															group.setReport(true);
														}
				 return rowNum;
			      
			}  
			  
			
					
        		  
        	 
        	 });
          
			//Group group=groupList.get(0);
             LOGGER.info("Exiting from com.dci.service.AuthenticationServiceImpl.getPrivileges(String userId, String clientId)");
	       return group;
    }
    
}
