package com.dci.model;

import java.util.HashMap;

public class Group {
	String id;
	String name;
	String desc;
	
	public Group(String id,String name)
	{
		this.id=id;
		this.name=name;
		this.Text= this.initializePerm();
		this.Subhead= this.initializePerm();
		this.Footnote= this.initializePerm();
		this.Table= this.initializePerm();
		this.Title= this.initializePerm();
		this.Footnote_Mapping= this.initializePerm();
		this.Fund_Management= this.initializePerm();
		this.Document= this.initializePerm();
		
		
	}
	public Group()
	{
		this.Text= this.initializePerm();
		this.Subhead= this.initializePerm();
		this.Footnote= this.initializePerm();
		this.Table= this.initializePerm();
		this.Title= this.initializePerm();
		this.Footnote_Mapping= this.initializePerm();
		this.Fund_Management= this.initializePerm();
		this.Document= this.initializePerm();
		
	}
	
	public HashMap<String, Boolean> initializePerm()
	{
		HashMap<String, Boolean> permMap = new HashMap<String, Boolean>(); 
		permMap.put("CREATE", false);
		permMap.put("EDIT", false);
		permMap.put("VIEW", false);
		return permMap;
	}
	public HashMap<String, Boolean> setPerm(HashMap <String, Boolean> permObj,String type, boolean val )
	{
		permObj.put(type, val);
		
	 return permObj;
	}
	

	
	
	private HashMap<String, Boolean> Text;
	private HashMap<String, Boolean> Subhead;
	private HashMap<String, Boolean> Footnote;
	private HashMap<String, Boolean> Table;
	private HashMap<String, Boolean> Title;
	private HashMap<String, Boolean> Footnote_Mapping;
	private HashMap<String, Boolean> Fund_Management;
	
	
	
	private boolean library;
	private boolean context;
	private boolean charts;
	private boolean openDataFile;
	private boolean uploadDataFile;
	
	private HashMap<String, Boolean> Document = new HashMap<String, Boolean>();
	private boolean changeStatus;
	private boolean carryForward;
	private boolean proof;
	
	private boolean inprogress;
	private boolean review;
	private boolean approved;
	private boolean published;
	private boolean revision;
	private boolean delete;
	
	
	private boolean variableManagement;
	private boolean serviceDesk; 
	private boolean Supplement;
	private boolean Report;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public boolean isLibrary() {
		return library;
	}
	public void setLibrary(boolean library) {
		this.library = library;
	}
	public boolean isContext() {
		return context;
	}
	public void setContext(boolean context) {
		this.context = context;
	}
	public boolean isCharts() {
		return charts;
	}
	public void setCharts(boolean charts) {
		this.charts = charts;
	}
	public boolean isOpenDataFile() {
		return openDataFile;
	}
	public void setOpenDataFile(boolean openDataFile) {
		this.openDataFile = openDataFile;
	}
	public boolean isUploadDataFile() {
		return uploadDataFile;
	}
	public void setUploadDataFile(boolean uploadDataFile) {
		this.uploadDataFile = uploadDataFile;
	}
	public boolean isChangeStatus() {
		return changeStatus;
	}
	public void setChangeStatus(boolean changeStatus) {
		this.changeStatus = changeStatus;
	}
	public boolean isCarryForward() {
		return carryForward;
	}
	public void setCarryForward(boolean carryForward) {
		this.carryForward = carryForward;
	}
	public boolean isProof() {
		return proof;
	}
	public void setProof(boolean proof) {
		this.proof = proof;
	}
	public boolean isVariableManagement() {
		return variableManagement;
	}
	public void setVariableManagement(boolean variableanagement) {
		this.variableManagement = variableanagement;
	}
	public boolean isServiceDesk() {
		return serviceDesk;
	}
	public void setServiceDesk(boolean serviceDesk) {
		this.serviceDesk = serviceDesk;
	}
	public boolean isSupplement() {
		return Supplement;
	}
	public void setSupplement(boolean supplement) {
		Supplement = supplement;
	}
	public boolean isReport() {
		return Report;
	}
	public void setReport(boolean report) {
		Report = report;
	}
	


	public boolean isInprogress() {
		return inprogress;
	}

	public void setInprogress(boolean inprogress) {
		this.inprogress = inprogress;
	}

	public boolean isReview() {
		return review;
	}

	public void setReview(boolean review) {
		this.review = review;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public boolean isRevision() {
		return revision;
	}

	public void setRevision(boolean revision) {
		this.revision = revision;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public HashMap<String, Boolean> getText() {
		return Text;
	}

	public void setText(HashMap<String, Boolean> text) {
		Text = text;
	}

	public HashMap<String, Boolean> getSubhead() {
		return Subhead;
	}

	public void setSubhead(HashMap<String, Boolean> subhead) {
		Subhead = subhead;
	}

	public HashMap<String, Boolean> getFootnote() {
		return Footnote;
	}

	public void setFootnote(HashMap<String, Boolean> footnote) {
		Footnote = footnote;
	}

	public HashMap<String, Boolean> getTable() {
		return Table;
	}

	public void setTable(HashMap<String, Boolean> table) {
		Table = table;
	}

	public HashMap<String, Boolean> getTitle() {
		return Title;
	}

	public void setTitle(HashMap<String, Boolean> title) {
		Title = title;
	}

	public HashMap<String, Boolean> getFootnote_Mapping() {
		return Footnote_Mapping;
	}

	public void setFootnote_Mapping(HashMap<String, Boolean> footnote_Mapping) {
		Footnote_Mapping = footnote_Mapping;
	}

	public HashMap<String, Boolean> getFund_Management() {
		return Fund_Management;
	}

	public void setFund_Management(HashMap<String, Boolean> fund_Management) {
		Fund_Management = fund_Management;
	}

	public HashMap<String, Boolean> getDocument() {
		return Document;
	}

	public void setDocument(HashMap<String, Boolean> document) {
		Document = document;
	}

}
