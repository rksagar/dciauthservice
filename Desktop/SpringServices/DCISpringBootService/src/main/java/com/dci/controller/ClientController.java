package com.dci.controller;

import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.dci.model.Group;
import com.dci.service.AuthenticationServiceImpl;
import com.dci.service.ClientInfoServicesImpl;
import com.dci.service.ClientServiceImpl;
import com.dci.service.JwtTokenServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@RestController
public class ClientController {
	private static final Logger LOGGER = LogManager.getLogger(ClientController.class);
	@Autowired
	private ClientServiceImpl csobj;
	@Autowired
	private ClientInfoServicesImpl clientInfoServicesImpl;

	@Autowired
	private AuthenticationServiceImpl authServObj;

	@GetMapping("/DCI/clientpost/")
	public Object getClients() {

		return csobj.getClients();
	}

	@PostMapping("/DCI/auth/login/")
	public Object login(@RequestParam("userId") String userId, @RequestParam("password") String password,
			@RequestParam("clientId") String clientId) {
		
		LOGGER.info(
				"Entering into com.dci.controller.ClientController.login(String userId, String password, String clientId)");
		String data = null;
		Map<String,Object> resultMap=new HashMap();
		Map<String, String> dataMap = new HashMap<String, String>();
		if(!userId.equals("") && !password.equals("") && !clientId.equals(""))
			
		{
			Map clientInfoMap = clientInfoServicesImpl.getClientInfo(userId, clientId);
			if(clientInfoMap!=null && !clientInfoMap.containsKey("HFMSG"))
			{
			   
				if (userId != null && password != null) {
					Map authenticationDetails = authServObj.authenticateUser(userId, password);
					if (authenticationDetails != null && authenticationDetails.get("HFMSGID") != null) {
		
						String msgId = authenticationDetails.get("HFMSGID").toString();
						if (msgId != null) {
							if ((msgId.trim().equals("-1") || msgId.trim().equals("2"))) {
								data = "Invalid password";
							} else if ((msgId.trim().equals("-2") || msgId.trim().equals("3"))) {
								data = "Invalid user";
							} else if (msgId.trim().equals("5")) {
								data = "Force Change Password";
							} else if (msgId.trim().equals("6")) {
								data = "User locked";
							} else if (msgId.trim().equals("99")) {
								data = "User/password expired";
							} else if (msgId.trim().equals("7")) {
								data = "User deactivated";
							} else if (msgId.trim().equals("0")) {
								data = "success";
								
							
								  Group group=(Group) authServObj.getPrivilegeInfo(userId,clientId);
									JwtTokenServiceImpl jwtToken = new JwtTokenServiceImpl();
									String token = jwtToken.getJwtToken(userId, authenticationDetails, clientInfoMap,group);
									dataMap.put("Token", token);
								}
							}
						}
						
					}
			}
			
			else {
				
				//dataMap.put("msg",clientInfoMap.get("HFMSG").toString());
				data=clientInfoMap.get("HFMSG").toString();
			}
			  
			 if(data.equalsIgnoreCase("Success"))
			 {
				// dataMap.put("message", data);
				 resultMap.put(data, dataMap);
			 }
			else
			{
				dataMap.put("message", data);
				resultMap.put("Failed", dataMap);
			}
		LOGGER.info("Exiting from com.dci.controller.ClientController.login(String userId, String password, String clientId)");
		return resultMap;
		}
		else {
			return resultMap.put("Failed","Please enter vaild input");
		}
	 }
	

	@PostMapping("/DCI/tokenpost/")
	public Object getToken(@RequestParam String token) {

		final String uri = "http://192.168.1.23:8080/jwt/{token}";

		Map<String, String> params = new HashMap<String, String>();
		params.put("token", token);

		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(uri, String.class, params);
		if (result.equalsIgnoreCase("valid")) {
			return csobj.getClients();
		} else {
			return null;
		}

	}
	@PostMapping("/DCI/accessprivilege")
	public Object getPrivilegeInfo(@RequestParam("userId") String userId,@RequestParam("clientId") String clientId)
	{
		
		
		String json="";
		Group group=(Group)authServObj.getPrivilegeInfo(userId,clientId);
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
		try {
			 json= ow.writeValueAsString(group);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return json;
		
	}

}

//}