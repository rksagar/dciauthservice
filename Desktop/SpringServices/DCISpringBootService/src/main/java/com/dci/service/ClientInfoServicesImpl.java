package com.dci.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dci.dao.ServiceDao;

@Service
public class ClientInfoServicesImpl implements ClientInfoServices {
	@Autowired
	private ServiceDao cdao;

	@Override
	public Map<String, String> getClientInfo(String userId, String clientId) {

		return cdao.getClientInfo(userId, clientId);
	}

}
