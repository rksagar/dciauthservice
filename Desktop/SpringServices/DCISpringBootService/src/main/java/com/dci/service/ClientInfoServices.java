package com.dci.service;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public interface ClientInfoServices {
	public Map<String, String> getClientInfo(String userId, String clientId);
}
