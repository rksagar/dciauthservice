package com.dci.dao;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.dci.DciSpringBootServiceApplication;
import com.dci.model.Group;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

@Repository
public class ServiceDao {
	private static final Logger LOGGER = LogManager.getLogger(ServiceDao.class);

	private SimpleJdbcCall jdbcCall;
	@Autowired
	private DataSource dataSource;

	Map authenticationDetails = new HashMap();
	
	public Map<String, String> getUserSaltDetails(String userId) {
		LOGGER.info("Entering into com.dci.dao.ClientDao.getUserSaltDetails(String userId)");
		Map<String, String> map = new HashMap<String, String>();

		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put("HFUSER_ID", userId);
		LOGGER.info("call SP7_DSGETSALT (" + userId + ")");

		jdbcCall = new SimpleJdbcCall(dataSource).withSchemaName("ZDBXAUTH04").withProcedureName("SP7_DSGETSALT")
				.declareParameters(new SqlParameter("HFUSER_ID", Types.VARCHAR));
		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
		Map<String, Object> out = jdbcCall.execute(in);

		Iterator<Entry<String, Object>> it = out.entrySet().iterator();
		while (it.hasNext() && it != null) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
			String key = (String) entry.getKey();
			Object value = entry.getValue();
			//LOGGER.info("key : " + key);
			//LOGGER.info("value : " + value);

			String result = value.toString().replaceAll("[{\\[\\]}]", "");

			/* userSaltMap=(Map)value; */
			String[] splited = result.split(",");
			if (!value.toString().equalsIgnoreCase("[]")) {
				for (String str : splited) {
					String[] arr = str.split("=");

					map.put(arr[0].trim(), arr[1].trim());
				}
			} else {
				map.put("FSALT", "");
				map.put("FUSERID", "0");
				map.put("FRESET_IND", "0");
				map.put("FACCOUNT_STATUS", "0");

			}
			LOGGER.info("Exiting from com.dci.dao.ClientDao.getUserSaltDetails(String userId)");
		}
		return map;
	}

	public String getUserSalt(String userId) {
		LOGGER.info("Entering into com.dci.dao.ClientDao.getUserSalt(String userId)");
		String salt = "";
		Map userSaltMap = getUserSaltDetails(userId);
		if (userSaltMap.containsKey("FSALT")) {
			salt = (String) userSaltMap.get("FSALT");
		}
		LOGGER.info("Existing from com.dci.dao.ClientDao.getUserSalt(String userId)");
		return salt;
	}

	public Map authenticateUserDB(String username, String password) {
		LOGGER.info("Entering into com.dci.dao.ClientDao.authenticateUserDB(String username, String password)");
		

		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put("HFUSER_ID", username);
		inParamMap.put("HFPWORD", password);
		LOGGER.info("call SP7_DSLOGIN(" + username + "," + password + ")");
		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
		jdbcCall = new SimpleJdbcCall(dataSource).withSchemaName("ZDBXAUTH04").withProcedureName("SP7_DSLOGIN")
				.declareParameters(new SqlParameter("HFUSER_ID", Types.VARCHAR),
						new SqlParameter("HFPWORD", Types.VARCHAR));
		Map<String, Object> out = jdbcCall.execute(in);

		Iterator<Entry<String, Object>> it = out.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
			String key = (String) entry.getKey();
			Object value = (Object) entry.getValue();

			LOGGER.info("key : " + key);
			LOGGER.info("value : " + value);
			authenticationDetails.put(key, value);
		}
		LOGGER.info("Exisiting from com.dci.dao.ClientDao.authenticateUserDB(String username, String password)");
		return authenticationDetails;
	}

	public Map<String, String> getClientInfo(String userId, String clientId) {
		LOGGER.info("Entering into com.dci.dao.ClientDao.getClientInfo(String userId, String clientId)");
		Map<String, String> clientInfoMap = new HashMap<String, String>();

		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put("HFUSER_ID", userId);
		inParamMap.put("HFCLIENTID", clientId);
		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
		LOGGER.info("call SP8_GETCLIENTINFOVALIDATE888(" + userId + "," + clientId + ")");
		jdbcCall = new SimpleJdbcCall(dataSource).withSchemaName("ZDBXAUTH04").withProcedureName("SP8_GETCLIENTINFOVALIDATE888")
				.declareParameters(new SqlParameter("HFUSER_ID", Types.VARCHAR),new SqlOutParameter("HFMSG", Types.VARCHAR),
						new SqlParameter("HFCLIENTID", Types.BIGINT));
		Map<String, Object> out = jdbcCall.execute(in);
		Iterator<Entry<String, Object>> it = out.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
			String key = (String) entry.getKey();
			Object value = (Object) entry.getValue();
		
		    if(out.containsKey("#result-set-1"))
			  { 
				  
				  String result = value.toString().replaceAll("[{\\[\\]}]", "");
				  String[] splited = result.split(","); 
				  if (!value.toString().equalsIgnoreCase("[]")) 
				  {
					  for (String str : splited)
					  {
						 
					   String[] arr = str.split("=");
					  LOGGER.info("key :-------------> " + arr[0].trim());
					  LOGGER.info("value :-------------> " + arr[1].trim());
					  clientInfoMap.put(arr[0].trim(), arr[1].trim()); 
					 
					  }
				  }
				//  getAccessPrivileges(userId,clientId);
			  }
			  else if(out.containsKey("#update-count-1"))
			  {
				 
				  if(out.containsKey("HFMSG")) 
				  {
					  
				    clientInfoMap.put("HFMSG", (String)out.get("HFMSG"));
				  } 
			}
				 
				  LOGGER.info("Exisiting from the com.dci.dao.ClientDao.getClientInfo(String userId, String clientId)");
				 
			 return clientInfoMap; 
			
		}
		LOGGER.info("Exisiting from the com.dci.dao.ClientDao.getClientInfo(String userId, String clientId)");
	
		return null;

	}


	public Object getClientList() {

		try {
			LOGGER.info("Entering into com.dci.dao.ClientDao.getClientList()");
			Object value = null;

			jdbcCall = new SimpleJdbcCall(dataSource).withSchemaName("ZDBXLM0005").withProcedureName("SP_UTGETCLIENTLIST").declareParameters(new SqlParameter("HFUSER_ID", Types.VARCHAR));
			Map<String, Object> inParamMap = new HashMap<String, Object>();
			inParamMap.put("HFUSER_ID", "dcisupportlm");

			SqlParameterSource in = new MapSqlParameterSource(inParamMap);

			Map<String, Object> out = jdbcCall.execute(in);

			Iterator<Entry<String, Object>> it = out.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
				String key = (String) entry.getKey();
				value = (Object) entry.getValue();

			
			}

			
			LOGGER.info("Exisiting from com.dci.dao.ClientDao.getClientList()");
			return value;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
