package com.dci.service;

import java.util.Map;

import com.dci.model.Group;

public interface JwtTokenService {

	public String getJwtToken(String userId, Map authenticationDetails, Map clientInfoMap,Group group);

	
}
