package com.dci.model;

public class GroupPrivilegesBean {

	
	private String objectSchemaDesc;
	private String privilegeOptionDesc;
	
	
	public void setObjectSchemaDesc(String objectSchemaDesc) {
		this.objectSchemaDesc = objectSchemaDesc;
	}
	public String getObjectSchemaDesc() {
		return objectSchemaDesc;
	}
	
	public String getPrivilegeOptionDesc() {
		return privilegeOptionDesc;
	}
	public void setPrivilegeOptionDesc(String privilegeOptionDesc) {
		this.privilegeOptionDesc = privilegeOptionDesc;
	}
/*	
	private String objectSchemaId;
	private String dciObjectId;
	private String dciObjectDesc;
	private String privilegeId;
	private String privilegeOptionId;
	
	private String privilegeOptionDisplayType;
	
	private String selectedInd;
	
	private String privilegeOptionDepName;
	private String privilegeOptionDepDesc;
	private String privilegeOptionDepDisplayType;
	private String privilegeOptionDeptId;
	
	private String selectedDeptInd;
	private String objectSchemaSeq;
	private String objectSeq;
	private String privilegeOptionSeq;
	private String privilegeOptionDepSeq;
	
	public String getDciObjectDesc() {
		return dciObjectDesc;
	}
	public void setDciObjectDesc(String dciObjectDesc) {
		this.dciObjectDesc = dciObjectDesc;
	}
	public String getDciObjectId() {
		return dciObjectId;
	}
	public void setDciObjectId(String dciObjectId) {
		this.dciObjectId = dciObjectId;
	}
	
	
	public String getObjectSchemaId() {
		return objectSchemaId;
	}
	public void setObjectSchemaId(String objectSchemaId) {
		this.objectSchemaId = objectSchemaId;
	}
	public String getObjectSchemaSeq() {
		return objectSchemaSeq;
	}
	public void setObjectSchemaSeq(String objectSchemaSeq) {
		this.objectSchemaSeq = objectSchemaSeq;
	}
	public String getObjectSeq() {
		return objectSeq;
	}
	public void setObjectSeq(String objectSeq) {
		this.objectSeq = objectSeq;
	}
	public String getPrivilegeId() {
		return privilegeId;
	}
	public void setPrivilegeId(String privilegeId) {
		this.privilegeId = privilegeId;
	}
	public String getPrivilegeOptionDepDesc() {
		return privilegeOptionDepDesc;
	}
	public void setPrivilegeOptionDepDesc(String privilegeOptionDepDesc) {
		this.privilegeOptionDepDesc = privilegeOptionDepDesc;
	}
	public String getPrivilegeOptionDepDisplayType() {
		return privilegeOptionDepDisplayType;
	}
	public void setPrivilegeOptionDepDisplayType(
			String privilegeOptionDepDisplayType) {
		this.privilegeOptionDepDisplayType = privilegeOptionDepDisplayType;
	}
	public String getPrivilegeOptionDepName() {
		return privilegeOptionDepName;
	}
	public void setPrivilegeOptionDepName(String privilegeOptionDepName) {
		this.privilegeOptionDepName = privilegeOptionDepName;
	}
	public String getPrivilegeOptionDepSeq() {
		return privilegeOptionDepSeq;
	}
	public void setPrivilegeOptionDepSeq(String privilegeOptionDepSeq) {
		this.privilegeOptionDepSeq = privilegeOptionDepSeq;
	}
	public String getPrivilegeOptionDeptId() {
		return privilegeOptionDeptId;
	}
	public void setPrivilegeOptionDeptId(String privilegeOptionDeptId) {
		this.privilegeOptionDeptId = privilegeOptionDeptId;
	}
	
	public String getPrivilegeOptionDisplayType() {
		return privilegeOptionDisplayType;
	}
	public void setPrivilegeOptionDisplayType(String privilegeOptionDisplayType) {
		this.privilegeOptionDisplayType = privilegeOptionDisplayType;
	}
	public String getPrivilegeOptionId() {
		return privilegeOptionId;
	}
	public void setPrivilegeOptionId(String privilegeOptionId) {
		this.privilegeOptionId = privilegeOptionId;
	}
	public String getPrivilegeOptionSeq() {
		return privilegeOptionSeq;
	}
	public void setPrivilegeOptionSeq(String privilegeOptionSeq) {
		this.privilegeOptionSeq = privilegeOptionSeq;
	}
	public String getSelectedDeptInd() {
		return selectedDeptInd;
	}
	public void setSelectedDeptInd(String selectedDeptInd) {
		this.selectedDeptInd = selectedDeptInd;
	}
	public String getSelectedInd() {
		return selectedInd;
	}
	public void setSelectedInd(String selectedInd) {
		this.selectedInd = selectedInd;
	}
	*/
}
