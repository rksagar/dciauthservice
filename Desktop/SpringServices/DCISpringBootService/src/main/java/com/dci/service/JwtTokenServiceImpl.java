package com.dci.service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.dci.model.Group;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtTokenServiceImpl implements JwtTokenService {

	@Override
	public String getJwtToken(String userId, Map authenticationDetails, Map clientInfoMap,Group group) {

		int ttlMillis = 800000;
		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long timeMilli = System.currentTimeMillis();
		Date now = new Date(timeMilli);

		// We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("mysecret");
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		// setting the JWT Claims
		String msgId = authenticationDetails.get("HFMSGID").toString();
		String msg = authenticationDetails.get("HFMSG").toString();
		String internalUserID = authenticationDetails.get("HFUSERID").toString();
		String clientId = clientInfoMap.get("FDEPTID").toString();
		String client_name = clientInfoMap.get("FDEPT_NAME").toString();
		Map map = new HashMap<String, Object>();
		map.put("alg", "HS256");
		map.put("typ", "JWT");
		Date exp = null;
		if (ttlMillis >= 0) {
			long expMillis = timeMilli + ttlMillis;
			exp = new Date(expMillis);

		}

		String builder = Jwts.builder().setId(internalUserID).setHeader(map).setExpiration(exp).setIssuedAt(now)
				.setNotBefore(now).claim("userId", userId).claim("msgId", msgId).claim("clientId", clientId)
				.claim("client_name", client_name).claim("msg", msg).claim("Permissions",group).signWith(SignatureAlgorithm.HS256, signingKey)
				.compact();
		return builder;

	}

}
